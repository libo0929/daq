void eventBuilder()
{
	gStyle->SetOptFit();

	// use TChain if multiple files
	TFile* f = new TFile("data.root");
	
    TNtuple* ntuple = (TNtuple*)f->Get("data");

	int event;
	int ch;
	std::vector<double>* wf = new std::vector<double>;

	ntuple->SetBranchAddress("event",  &event);
	ntuple->SetBranchAddress("ch",  &ch);
	ntuple->SetBranchAddress("wf",  &wf);

	TH1F* hAmp = new TH1F("amp", "amp", 100, -0.2, 2.);

    for(int i=0; i<ntuple->GetEntries(); i++)
    {
        ntuple->GetEntry(i);

		//cout << "event: " << event << ", ch: " << ch << ", pulse: " << wf->size() << endl;

		auto ampIter = std::max_element(wf->begin(), wf->end());

		hAmp->Fill(*ampIter);
	}

	hAmp->GetXaxis()->SetTitle("pulse amplitude (V)");
	hAmp->GetYaxis()->SetTitle("Entries");
	hAmp->Draw();
}
