/////////////////////////////////////////////////////////////////////
//g++ fileConverter.C `root-config --cflags` `root-config --glibs`
/////////////////////////////////////////////////////////////////////

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "TFile.h"
#include "TTree.h"

using namespace std;

int main(int argc, char *args[])
{
	cout << args[1] << endl;

	// for the date
	string date = args[1];
	date.erase(0, date.find("_Data_"));
	date.erase(0, 6);
	date.erase(date.find("_Ascii"));

	cout << date << endl;

    // for the file index
	string fileIndex = args[1];
	auto foundIndex = fileIndex.find("dat_");

	if(foundIndex == string::npos)
	{
		fileIndex = "0000";
	}
	else
	{
		fileIndex.erase(0, fileIndex.find("dat_"));
		fileIndex.erase(0, 4); // erase the string of 'dat_'
	}

	cout << fileIndex << endl;

	//ifstream rf("Run_mu150000_Data_3_16_2022_Ascii/Run_mu150000_Data_3_16_2022_Ascii.dat_0162");
	ifstream rf(args[1]);

	string fileName = "data_" + date + "_" + fileIndex + ".root";

	TFile* rootFile = new TFile(fileName.c_str(), "recreate");

	TTree* tree = new TTree("data", "detData");

	int event;
	int ch;
	std::vector<double> wf;

	tree->Branch("event", &event);
	tree->Branch("ch", &ch);
	tree->Branch("wf", "std::vector<double>", &wf); 

	// the head
	for(int i = 0; i < 4; ++i)
	{
		string os;
		getline(rf, os);
	}

	int N = 500;

#if 0
	TCanvas* c1 = new TCanvas("c1","Hits",10, 10, 600, 600);
	TH1F* h = new TH1F("", "", 1024, 0, 1024);
#endif

	for(int iEvt = 0; iEvt < N; ++iEvt)
	{
		// loop the lines of an event
		for(int i = 0; i < 4; ++i)
		{
			string os;
			getline(rf, os);

			string s = os;

			bool foundEvt = ( s.find("=== EVENT ") != std::string::npos );
			bool foundCH = ( s.find("=== CH: ") != std::string::npos );
			bool foundWaveform = ( s.find("===") == std::string::npos );
			bool foundTDCTime = ( s.find("TDC Corrected =") == std::string::npos );

			// event
			if(foundEvt)
			{ 
				s.erase(0, s.find_first_not_of("=== EVENT "));
				s.erase(s.find_first_of(" ==="));
				
				//cout << "---> Event: " << std::atoi(s.c_str()) << endl;

				event = std::atoi(s.c_str());
			}

			// CH
			if(foundCH)
			{ 
				s.erase(0, s.find_first_not_of("=== CH "));
				s.erase(s.find_first_of(" "));
				
				//cout << "     CH: " << std::atoi(s.c_str()) << endl;

				ch = std::atoi(s.c_str());
			}

			// this is the waveform !
			if(foundWaveform)
			{
				istringstream iss(s);
				string amp;

				int iData = 0;

				while( iss >> amp)
				{
					//cout << "     data: " << amp << endl;
					wf.push_back( std::stof(amp) );
#if 0
					h->Fill(iData++, std::stof(amp) );
#endif
				}

				tree->Fill();

				wf.clear();

#if 0
				h->Draw("hist");
#endif
			}
		}

#if 0
		c1->Update();

		char c = getchar();

		if(c == 'q')
			break;
		else
			h->Reset();  
#endif
	}

	tree->Write();
	rootFile->Close();

	rf.close();
}
