{
	TH1F* h = new TH1F("", "", 20, 0, 2.56);

	int n = 0;

	for(int i = 0; i < 100000000; ++i)
	{
		float t = gRandom->Exp(2.2);

		if(t < 2.56)
		{
			h->Fill(gRandom->Exp(2.2));
			++n;
		}

		if(n>2000) break;
	}

	//h->GetYaxis()->SetRangeUser(0, 80);
	h->Draw();
}
