dirIndex=2

baseDir="/media/libo/HD2/muonExp"
dirs=`ls ${baseDir}/RunMu${dirIndex}`

convDir="convertedRun${dirIndex}"
mkdir -p ${baseDir}/${convDir}

convertedFileDir="${baseDir}/${convDir}"

i=0

for dir in ${dirs[@]}; do
	echo ${baseDir}/RunMu${dirIndex}/${dir}
	files=`ls ${baseDir}/RunMu${dirIndex}/${dir}/Run_*`

	for file in ${files}; do
		echo ${file}
		./a.out ${file}
		totalIndex=`printf %08d ${i}`
		fileName=`ls *.root`
		mv ${fileName} ${convertedFileDir}/${fileName}_${totalIndex}

		i=`expr $i + 1`
	done
done
